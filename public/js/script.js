"use strict";
;(function (window, document, $) {
	 /**
     * JS class to give a extra elements control in admin iugu section 
     * @author Rafael Caparroz Zuim <rafael.czuim@gmail.com> 
	**/	
	function AdminControl(){
		
		/*
		 * Listainers da tela	
		*/
		this.binds = function(){
			var self = this;
			$("body").on("submit","#form-pessoas",function(e){				
				e.preventDefault();
				var id =$("#id_pessoa").val();
				$.ajax({ 
					type: 'POST', 
					url: '/pessoas/add',
					dataType : "json",
					data: {
						"nome":$("#nome_pessoa").val(),
						"sobrenome":$("#sobrenome_pessoa").val(),
						"endereco":$("#endereco_pessoa").val(),
						"id":id // se tem id ele edita se não tem insere ;)
						
					},
					success: function (data) { 
						self.cleanFields();
						if(data.success && id == ""){
							self.inserRow(data.trData);
						}else if(data.success){
							$("tr#"+id).remove();
							self.inserRow(data.trData);	
							self.cleanFields();
							$("#btn-submit").text("Adicionar");
							$("#btn-cancelar").remove();
						}
					},
					error : function(jqXHR,textStatus,errorThrown){					
						console.log(jqXHR);
						console.log(textStatus);
						console.log(errorThrown);
					}
					
				});
			})
			// bind no botão excluir
			$("body").on("click","a.excluir",function(){				
				var id = $(this).data("id");
				var tr = $(this).closest("tr");				
				$.ajax({ 
					type: 'POST', 
					url: '/pessoas/delete/' + id,					
					success: function (data) { 
						tr.remove();
					},
					error : function(jqXHR,textStatus,errorThrown){					
						console.log(jqXHR);
						console.log(textStatus);
						console.log(errorThrown);
					}					
				});
			})
			// bind no botao editar
			$("body").on("click","a.editar",function(){				
				var id = $(this).data("id");				
				$.ajax({ 
					type: 'POST', 
					url: '/pessoas/edit/' + id,
					dataType : "json",
					success: function (data) { 
						self.editRow(data);
					},
					error : function(jqXHR,textStatus,errorThrown){					
						console.log(jqXHR);
						console.log(textStatus);
						console.log(errorThrown);
					}
					
				})
			});
			
			// cancela a edição no form
			$("body").on("click","#btn-cancelar",function(){
				self.cleanFields();
				$("#btn-submit").text("Adicionar");
				$(this).remove();
			})
			
			
		}
		
		
		this.cleanFields = function(){
			$("#nome_pessoa").val("");
			$("#sobrenome_pessoa").val("");
			$("#endereco_pessoa").val("");
			$("#id_pessoa").val("");
		}
		/** insere uma nova linha na tabela **/
		this.inserRow = function(data){
			$("#tb-pessoas tbody").append(
					$("<tr/>")
						.attr("id",data.id)
						.append(
							$("<td/>")
								.text(data.nome)
						)
						.append(
							$("<td/>")
								.text(data.sobrenome)
						)
						.append(
							$("<td/>")
								.text(data.endereco)
						)
						.append(
							$("<td/>")
								.append(
									$("<a/>")
										.attr("href","#")
										.attr("data-id",data.id)
										.attr("title","Editar registro")
										.addClass("editar")
										.append(
											$("<i/>")
												.addClass("glyphicon glyphicon-pencil")
											)
								)
								.append(
										$("<a/>")
										.attr("href","#")
										.attr("data-id",data.id)
										.attr("title","Excluir registro")
										.addClass("excluir")
										.append(
											$("<i/>")
												.addClass("glyphicon glyphicon-remove")
										)
								)																								
						)
						
			)
		}
		
		//carrega no formulário os dados para edição
		this.editRow = function(data){
			$("#nome_pessoa").val(data.Pessoa.nome);
			$("#sobrenome_pessoa").val(data.Pessoa.sobrenome);
			$("#endereco_pessoa").val(data.Pessoa.endereco);
			$("#id_pessoa").val(data.Pessoa.id);
			$("#btn-submit").text("Alterar");
			if($("#btn-cancelar").length == 0){
				$("#btn-submit").parent().append(
						$("<button/>")
							.text("Cancelar")
							.addClass("btn btn-warning")
							.attr("id","btn-cancelar")					
				)
			}			
		}
		

	}
	
	// Initializes the class
    $(document).ready(
            function(){            	
                window.AdminControl = AdminControl = new AdminControl();                
                AdminControl.binds();               
                
            }
    )
})(window, document, jQuery);