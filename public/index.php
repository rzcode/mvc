<?php
/** Framework MVC desenvolvido por Rafael Caparroz Zuim (HIRE ME !!!)
 Esta aplicação foi desenvolvida interiamente do zero. Seguindo o padrao PSR-4 com autoload
 das biblitecas(criadas por mim) que se encontram em vendor/core/
 A aplicação que utiliza estas bibliotecas se encontram em src/App
 */

//constantes
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));

// require direto do config
require ROOT . "/" . "src" . DS ."App" . DS ."Config". DS ."app.config.php";
require_once ROOT .DS .'vendor'.DS.'autoload.php';

// database
define("DB_HOST", $config["database"]["host"]);
define("DB_USER", $config["database"]["user"]);
define("DB_PASS", $config["database"]["password"]);
define("DB_NAME", $config["database"]["name"]);

// diretórios publicos
define("CSS_URL", '/css/');
define("JS_URL", '/js/');
define("FONTS_URL", '/fonts/');

use Mvc\Init;

// chama o handle que toma conta da request
$init = init::get_instance($config);

$init->requestHandle();
?>
