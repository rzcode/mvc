DROP TABLE IF EXISTS `pessoas`;
CREATE TABLE `pessoas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `sobrenome` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Agenda de contatos';


INSERT INTO pessoas (nome, sobrenome, endereco,telefone,celular,email)
VALUES ('Rosangela','da silva','Rua santo augostinho','(41)48184529','(99)999999999','rosangela@dominio.com.br');

INSERT INTO pessoas (nome, sobrenome, endereco,telefone,celular,email)
VALUES ('Rafael','da silva','Rua santo augostinho','(41)15439682','(99)999999999','rafael@dominio.com.br');

INSERT INTO pessoas (nome, sobrenome, endereco,telefone,celular,email)
VALUES ('Ricardo','sobrenome ','Rua santo augostinho','(41)98648215','(99)999999999','ricardo@dominio.com.br');

INSERT INTO pessoas (nome, sobrenome, endereco,telefone,celular,email)
VALUES ('Eduardo',' teste','Rua santo augostinho','(41)14362586','(99)999999999','eduardo@dominio.com.br');

INSERT INTO pessoas (nome, sobrenome, endereco,telefone,celular,email)
VALUES ('Cristina',' de almeida','Rua santo augostinho','(41)67979787','(99)999999999','cristina@dominio.com.br');

