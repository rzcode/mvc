<?php
/**
 * Created by PhpStorm.
 * User: rafael.zuim
 * Date: 09/09/16
 * Time: 15:45
 */
namespace Mvc;

class Database {
    protected $_dbHandle;
    protected $_result;
    
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    /** Conexão com o banco */

    function connect($address, $account, $pwd, $name) {
        $this->_dbHandle = @mysql_connect($address, $account, $pwd);
        if ($this->_dbHandle != 0) {
            if (mysql_select_db($name, $this->_dbHandle)) {            	
                return 1;
            }
            else {            	
                return 0;
            }
        }
        else {        	
            return 0;
        }
    }

    /** Fecha a conexão com o banco de dados **/

    function disconnect() {
        if (@mysql_close($this->_dbHandle) != 0) {
            return 1;
        }  else {
            return 0;
        }
    }

    function selectAll() {
        $query = 'select * from `'.$this->table.'`';        
        return $this->query($query);
    }

    function select($id) {
        $query = 'select * from `'.$this->table.'` where `id` = \''.mysql_real_escape_string($id).'\'';
        return $this->query($query, 1);
    }
    
    function add($fields =[]){    	
    	try{
	    	foreach($fields as $key=>$val){
	    		$keys[] = $key;
	    		$values[] = $this->sanitizeFileds($val);
	    	}    	    	
	    	$insert = "INSERT INTO {$this->table} (".implode(",",$keys).") VALUES (".implode(",",$values).")";	    	
	    	mysql_query($insert, $this->_dbHandle);
	    	return mysql_insert_id();
    	}catch(\Exception $e){
    		log($e->getMessage());
    	}
    }
    // altera um registro
    function edit($fields =[],$id){
    	try{    		
    		if($this->select($id)){
	    		foreach($fields as $key=>$val){	    			
	    			$values[] = $key . "= " . $this->sanitizeFileds($val);
	    		}
    		}
    		$update = "UPDATE {$this->table} SET ".implode(",",$values)." WHERE id={$id}";			
    		
    		mysql_query($update, $this->_dbHandle);
    		return $id;
    	}catch(\Exception $e){
    		log($e->getMessage());
    	}
    }    
    public function delete($id){
    	$delete = "DELETE FROM {$this->table} where id={$id}";
    	return mysql_query($delete, $this->_dbHandle);
    }  
    


    /** SQL Personalizada **/

    function query($query, $singleResult = 0) {

        $this->_result = mysql_query($query, $this->_dbHandle);

        if (preg_match("/select/i",$query)) {
            $result = array();
            $table = array();
            $field = array();
            $tempResults = array();
            $numOfFields = mysql_num_fields($this->_result);
            for ($i = 0; $i < $numOfFields; ++$i) {
                array_push($table,mysql_field_table($this->_result, $i));
                array_push($field,mysql_field_name($this->_result, $i));
            }
            while ($row = mysql_fetch_row($this->_result)) {
                for ($i = 0;$i < $numOfFields; ++$i) {
                    $table[$i] = trim(ucfirst($table[$i]),"s");
                    $tempResults[$table[$i]][$field[$i]] = $row[$i];
                }
                if ($singleResult == 1) {
                    mysql_free_result($this->_result);
                    return $tempResults;
                }
                array_push($result,$tempResults);
            }
            mysql_free_result($this->_result);
            return($result);
        }

    }

    /** Busca o numero de linhas executadas**/
    function getNumRows() {
        return mysql_num_rows($this->_result);
    }

    /** Libera os resousers da memoria **/

    function freeResult() {
        mysql_free_result($this->_result);
    }

    /** Retorna o erro **/

    function getError() {
        return mysql_error($this->_dbHandle);
    }
    // coloca aspas simples nas strings
    function sanitizeFileds($str){
    	return "'" . $str . "'";
    }
}
