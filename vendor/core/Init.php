<?php
/**
 * 
 * @author rafael
 *
 */

namespace Mvc;

class Init{
	
	protected static $instance = null;
	
	private $params = [];
	private $config = [];
	private $controllerSufix = "Ct";
	private $controllerPrefix = "Ct";
	
	private function __construct($config=[]){
		 $this->config = $config;
	}
	
	public static function get_instance($config=[]) {
		if (null == self::$instance) {
				self::$instance = new self ($config);
		}			
		return self::$instance;
	}
	// faz o randle da request para acionar o controller
	public function requestHandle() {
		$params =[];
		$uri = explode("/",trim($_SERVER["REQUEST_URI"]));
		
		
		// remove a primeira posição que sempre é vazia
		array_shift($uri);
		
		
		// aplica a lógica sobre a uri recebida para definir os controllers
		switch ($tamanho = sizeof($uri)){
			case $tamanho == 1 && $uri[0]=="":
				// controller e action defaults definidos no config
				$controller = ucfirst($this->config["default_route"]["controller"]);
				$action = $this->config["default_route"]["action"];
				break;
			case 1 :				
				$controller = ucfirst($uri[0]);
				$action = "index";
				break;
			case $tamanho >= 2 :
				$controller = ucfirst($uri[0]);
				$action = $uri[1];
				for ($x=2 ; $x < sizeof($uri) ; $x++) {
					$params[] = $uri[$x];
				}
				break;
		}
		$model = rtrim($controller);
		if(method_exists('\App\Controllers\\' . $controller . $this->controllerSufix, $action)){
			// montando o nome da classe junto com o namespace da aplicação
			$prepareControllerNmSpace = '\App\Controllers\\' . $controller . $this->controllerSufix;
			
			// executando o controller
			$dispatch = new $prepareControllerNmSpace($controller,$action,$model);
			
			if ((int)method_exists($prepareControllerNmSpace, $action)) {			
				call_user_func_array(array($dispatch,$action),$params);
			}
		}else{
			echo "<h1> Não tratei as excessões neste projeto, mas a rota informada não é acessível. ;)"; 
		}
	}
}
?>