<?php
/**
 * Created by PhpStorm.
 * User: rafael.zuim
 * Date: 09/09/16
 * Time: 14:55
 */

namespace Mvc;


class View
{
    protected $variables = [];
    protected $controller;
    protected $action;
    public $ajax = false;


    function __construct($controller,$action) {    	
        $this->controller = ucfirst($controller);
        $this->action = ucfirst($action);
    }

    /**
     * @param $name
     * @param $value
     * Variáveis que serão disponibilizadas na view
     */
    function set($name,$value) {
        $this->variables[$name] = $value;
    }

    /**
     * busca e exibe a view
     */

    function render() {
        extract($this->variables);
        if(!$this->ajax) {
            if (file_exists(
                ROOT . DS . 'src' . DS . 'App' . DS . 'Views' . DS . $this->controller . DS . 'header.php'
            )) {
                include(ROOT . DS . 'src' . DS . 'App' . DS . 'Views' . DS . $this->controller . DS . 'header.php');
            } else {
                include(ROOT . DS . 'src' . DS . 'App' . DS . 'Views' . DS . 'header.php');
            }            
            include(ROOT . DS . 'src' . DS . 'App' . DS . 'Views' . DS . $this->controller . DS . $this->action . '.php');

            if (file_exists(
                ROOT . DS . 'src' . DS . 'App' . DS . 'Views' . DS . $this->controller . DS . 'footer.php'
            )) {
                include(ROOT . DS . 'src' . DS . 'App' . DS . 'Views' . DS . $this->controller . DS . 'footer.php');
            } else {
                include(ROOT . DS . 'src' . DS . 'App' . DS . 'Views' . DS . 'footer.php');
            }
        }
    }
}