<?php
/**
 * Superclasse que implementa a gerência das rotas
 */
namespace Mvc;
use Mvc\View;

class Controller {
	protected $controllerName;
	protected $actionName;
	protected $view;
	protected $pageTitle = "Agenda de Contatos";
	 
	
	private $model = '';
	//private $params = [];
	private $sufix = 'Ct';
	private $ajax = false;

	
	public function __construct($controller,$action,$model){

		$this->controllerName = $controller;
		$this->actionName = $action;
		$this->model = $model;
		$this->view = new View($this->controllerName,$this->actionName);
		// prepara com o namespace da aplicação
		$prepareModelNmSpace = '\App\Models\\' . $model;
		$this->$model =new $prepareModelNmSpace($model);
	}	

	protected function set($name,$value) {
		$this->view->set($name,$value);
	}
	
	protected function setPageTitle($pageTitle){
		$this->pageTitle = $pageTitle;
	}
	
	protected function getPageTitle(){
		return $this->pageTitle;
	}
	
	protected function setAjaxRequest($ajaxRequest = true){
		$this->ajax = $ajaxRequest;
	}
	// busca da resisição o valor informado
	public function data($key="",$default = ""){
		if(array_key_exists($key, $_REQUEST)){
			return $_REQUEST[$key];					
		}else{
			return $default;
		}
	}

	public function __destruct() {
		$this->set("pageTitle", $this->pageTitle);
		$this->view->ajax = $this->ajax;
		$this->view->render();
	}
	
	
}
?>