<?php
namespace App\Controllers;
use Mvc\Controller;

class PessoasCt extends Controller{
	
	public function Index(){		
		$this->setPageTitle("Agenda de contatos");
		$this->set("pessoas", $this->Pessoas->selectAll());
	}
	
	public function ReloadData(){
		$this->setAjaxRequest();
		
	}
	public function Add(){
		$this->setAjaxRequest(true);

		$data = [	"nome"=>$this->data("nome"),
					"sobrenome"=>$this->data("sobrenome"),
					"endereco"=>$this->data("endereco")];

		if(!$this->data("id")){
			// inserindo
			$id = $this->Pessoas->add($data);
		}else{
			// alterando
			$id = $this->Pessoas->edit($data,$this->data("id"));
			
		}
		if(!$id){
			echo json_encode(["success" => false]);
		}else{
			$data["id"] = $id;
			echo json_encode(["success" => true,"trData" => $data]);
		}
		die();
	}
	// recebe o id e remove do banco
	public function Delete($id){
		$this->setAjaxRequest(true);
				
		$success = $this->Pessoas->delete($id);		
		echo json_encode(["success" => 1, "id" => $id]);
		die();
	}
	//recebe o id e retorna os dados para edição
	public function Edit($id){
		$this->setAjaxRequest(true);
		$pessoa = $this->Pessoas->select($id);
		if(!$pessoa){
			echo json_encode(["success" => false]);
		}else{		
			echo json_encode(array_merge(["success" => true],$pessoa));
		}
		die();
	}
	
	
}
