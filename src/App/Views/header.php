<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $pageTitle ?></title>
    <meta name="description" content="Teste Desenvolvedor">
    <meta name="author" content="Rafael Caparroz Zuim">

    <link rel="stylesheet" href="<?php echo CSS_URL ?>style.css?v=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="<?=FONTS_URL?>open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
</head>
<body>
<section class="container" id="main">

