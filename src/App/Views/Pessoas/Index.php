<div class="col-md-12">
	<h2 class="mb-20">Agenda de contatos</h2>
	<hr/>
	<form class="mt-20" method="POST" action="javascript:;" id="form-pessoas">
		<input name="pessoa[id]" id="id_pessoa" type="hidden"/>
		<div class="form-group">
            <div class="col-md-6">
                <label for="pessoa[nome]" class="control-label">Nome</label>
                <input type="text" class="form-control" name="pessoa[nome]" id="nome_pessoa"
                        placeholder="Nome da pessoa" required>

            </div>
            <div class="col-md-6">
                <label for="pessoa[sobrenome]" class="control-label">Sobrenome</label>
                <input type="text" class="form-control" name="pessoa[sobrenome]" id="sobrenome_pessoa"
                        placeholder="Sobrenome da pessoa" required>
            </div>
            <div class="col-md-12">
                <label for="pessoa[endereco]" class="control-label">Endereço</label>
                <input type="text" class="form-control" name="pessoa[endereco]" id="endereco_pessoa"
                       placeholder="Endereco da pessoa" required >
            </div>
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary" id="btn-submit">Adicionar</button>
            </div>
        </div>

	</form>
</div>
<div class="col-md-12">
<hr class="mt-20 mb-20"/>
</div>

<div class="col-md-12 mt-20">
	<table id="tb-pessoas" class="table table-striped">
		<thead>
			<th>Nome</th>
			<th class="d-none d-xl-block">Sobrenome</th>
			<th class="d-none d-xl-block">Endereço</th>
			<th class="d-none d-xl-block">Ações</th>
		</thead>
		<tbody>
		<?php
			foreach($pessoas as $row){
		?>
			<tr id="<?php echo $row["Pessoa"]["id"] ?>">
				<td ><?php echo $row["Pessoa"]["nome"] ?></td>
				<td class="d-none d-xl-block"><?php echo $row["Pessoa"]["sobrenome"] ?></td>
				<td class="d-none d-xl-block"><?php echo $row["Pessoa"]["endereco"] ?></td>
				<td class="d-none d-xl-block">
					<a href="javascript:;" class="editar btn btn-success" data-id="<?php echo $row["Pessoa"]["id"] ?>" title="Editar registro">
                        <span class="oi oi-pencil"></span>
					</a>
					<a href="javascirpt:;" class="excluir btn btn-danger" data-id="<?php echo $row["Pessoa"]["id"] ?>" title="Excluir registro">
                        <span class="oi oi-circle-x"></span>
					</a>
				</td>	
			</tr>
		<?php
			}
		?>
		</tbody>
	</table>  
</table>
</div>
