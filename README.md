**MCV feito por Rafael Caparroz Zuim**

Este repositório contem uma aplicação construída em cima de um estudo MVC feita por mim.

O core de minha aplicação está no padrão de autoload de namespaces PSR-4 

O core e a aplicação contém apenas o básico do MVC, e não possui nada que não tenha sido codificado por mim.

No frontend básico de minha aplicação utilizei jquery e bootstrap para montar um CRUD totalmente em AJAX.

Não foram tratados erros e este conteúdo é de caráter extremamente experimental e de estudo.